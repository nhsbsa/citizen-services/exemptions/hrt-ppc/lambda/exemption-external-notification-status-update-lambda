import {
  loggerWithContext,
  setCorrelationId,
} from "@nhsbsa/hrt-ppc-npm-logging";
import { SQSRecord } from "aws-lambda";
import { Channel } from "@nhsbsa/health-charge-exemption-npm-utils-rest/dist/src/clients/common";
import {
  getNotificationByExternalId,
  updateNotificationById,
} from "./api-utils/notification";
import { CALLBACK_NOTIFY_EVENT } from "./api-utils/constants";
import {
  GovNotifyStatus,
  NotificationCallback,
  NotificationPatchData,
  NotificationStatus,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

let logger = loggerWithContext();

export async function processRecord(record: SQSRecord) {
  logger.info(`Processing event [messageId: ${record.messageId}]`);

  const messageBody: NotificationCallback = validateRecordBody(record);
  const { id, status } = messageBody;
  setCorrelationId(id ? id : undefined);
  logger = loggerWithContext();
  const headers = buildHeadersWithCorrelationId(id);

  try {
    const { id: notificationId } =
      (await getNotificationByExternalId(id, headers)) || {};

    if (!notificationId) {
      throw new Error(`Could not find notification by external id`);
    }

    logger.info(
      `Notification [id: ${notificationId}] retrieved successfully for [externalNotifyId: ${id}]`,
    );

    const data: NotificationPatchData = {
      status: NotificationStatus.FAILED,
    };
    if (status === GovNotifyStatus.DELIVERED) {
      data.status = NotificationStatus.COMPLETED;
    }

    await updateNotificationById(notificationId, headers, data);
  } catch (error) {
    const message = `Notification has not been updated for [externalNotifyId: ${id}] with error: ${error.message}`;
    logger.error(message);
    throw error;
  }
}

function validateRecordBody(record: SQSRecord) {
  let messageBody: NotificationCallback;
  try {
    messageBody = JSON.parse(record.body);
  } catch (err) {
    const message = `Unable to parse record body: ${record.body} with error ${err}`;
    logger.error(message);
    throw new Error(message);
  }

  if (!messageBody.id || !messageBody.status) {
    const message = `Invalid record body, must contain Id and Status.`;
    logger.error(message);
    throw new Error(message);
  }

  return messageBody;
}

function buildHeadersWithCorrelationId(correlationId: string) {
  return {
    channel: Channel.BATCH,
    "user-id": CALLBACK_NOTIFY_EVENT,
    "correlation-id": correlationId,
  };
}
