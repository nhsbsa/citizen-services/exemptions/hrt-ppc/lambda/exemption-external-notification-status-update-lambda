import { SQSEvent } from "aws-lambda";
import { logger } from "@nhsbsa/hrt-ppc-npm-logging";
import { processRecord } from "./process-sqs-event-record";

export async function handler(event: SQSEvent) {
  try {
    logger.info("exemption-external-notification-status-update-lambda");
    const { Records: records } = event;

    await Promise.all(records.map((record) => processRecord(record)));
    return "Processing finished";
  } catch (err) {
    const message = err;
    logger.error(message);

    throw err;
  }
}
