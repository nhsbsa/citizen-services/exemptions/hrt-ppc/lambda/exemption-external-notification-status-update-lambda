import { processRecord } from "../process-sqs-event-record";
import {
  mockNotificationByExternalIdResponse,
  mockCallBackRecord,
} from "../__mocks__/mockData";
import * as notification from "../api-utils/notification";
import { logger } from "@nhsbsa/hrt-ppc-npm-logging";

let updateNotificationSpy: jest.SpyInstance;
let getNotificationByExternalIdSpy: jest.SpyInstance;
let winstonInfoLoggerSpy;
let winstonErrorLoggerSpy;
const internalServerError = new Error("Internal Server Error");

beforeEach(() => {
  // given
  winstonInfoLoggerSpy = jest.spyOn(logger, "info");
  winstonErrorLoggerSpy = jest.spyOn(logger, "error");

  getNotificationByExternalIdSpy = jest
    .spyOn(notification, "getNotificationByExternalId")
    .mockResolvedValue(mockNotificationByExternalIdResponse);

  updateNotificationSpy = jest
    .spyOn(notification, "updateNotificationById")
    .mockResolvedValue(); // returns Promise<void>
});

afterEach(() => {
  jest.resetAllMocks();
  jest.restoreAllMocks();
  jest.resetModules();
});

describe("processRecord()", () => {
  describe("should process the sqs record and patch notification", () => {
    afterEach(() => {
      // then
      expect(winstonErrorLoggerSpy).toHaveBeenCalledTimes(0);
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(2);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        1,
        `Processing event [messageId: messagex-idxx-xxxx-xxxx-xxxxxxxxxxxx]`,
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        2,
        "Notification [id: notifica-tion-idxx-xxxx-xxxxxxxxxxxx] retrieved successfully for [externalNotifyId: external-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
      );
    });

    it("should process the sqs record and patch with completed status when callback status is delivered", async () => {
      // given / when
      await expect(
        processRecord(buildCallBackMessage("delivered")),
      ).resolves.not.toThrow();

      // then
      expect(getNotificationByExternalIdSpy).toHaveBeenCalledTimes(1);
      expect(
        getNotificationByExternalIdSpy.mock.calls[0][0],
      ).toMatchInlineSnapshot(`"external-idxx-xxxx-xxxx-xxxxxxxxxxxx"`);
      expect(updateNotificationSpy).toHaveBeenCalledTimes(1);
      expect(updateNotificationSpy.mock.calls[0]).toMatchInlineSnapshot(`
        [
          "notifica-tion-idxx-xxxx-xxxxxxxxxxxx",
          {
            "channel": "BATCH",
            "correlation-id": "external-idxx-xxxx-xxxx-xxxxxxxxxxxx",
            "user-id": "CALLBCK_NOTIFY_EVENT",
          },
          {
            "status": "COMPLETED",
          },
        ]
        `);
    });

    it.each(["permanent-failure", "temporary-failure", "technical-failure"])(
      "should process the sqs record and patch with failed status when callback status is %s",
      async (status) => {
        // given / when
        await expect(
          processRecord(buildCallBackMessage(status)),
        ).resolves.not.toThrow();

        // then
        expect(getNotificationByExternalIdSpy).toHaveBeenCalledTimes(1);
        expect(
          getNotificationByExternalIdSpy.mock.calls[0][0],
        ).toMatchInlineSnapshot(`"external-idxx-xxxx-xxxx-xxxxxxxxxxxx"`);
        expect(updateNotificationSpy).toHaveBeenCalledTimes(1);
        expect(updateNotificationSpy.mock.calls[0]).toMatchInlineSnapshot(`
        [
          "notifica-tion-idxx-xxxx-xxxxxxxxxxxx",
          {
            "channel": "BATCH",
            "correlation-id": "external-idxx-xxxx-xxxx-xxxxxxxxxxxx",
            "user-id": "CALLBCK_NOTIFY_EVENT",
          },
          {
            "status": "FAILED",
          },
        ]
        `);
      },
    );
  });

  describe("should process the sqs record and throw error", () => {
    afterEach(() => {
      // then
      expect(winstonErrorLoggerSpy).toHaveBeenCalledTimes(1);
      expect(winstonErrorLoggerSpy).toHaveBeenCalledWith(
        `Notification has not been updated for [externalNotifyId: external-idxx-xxxx-xxxx-xxxxxxxxxxxx] with error: Internal Server Error`,
      );
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        1,
        `Processing event [messageId: messagex-idxx-xxxx-xxxx-xxxxxxxxxxxx]`,
      );
    });

    it("should throw error when getNotificationByExternalId in notification api throws error", async () => {
      // given
      getNotificationByExternalIdSpy = jest
        .spyOn(notification, "getNotificationByExternalId")
        .mockRejectedValue(internalServerError);

      // when
      await expect(
        processRecord(buildCallBackMessage("delivered")),
      ).rejects.toThrow(internalServerError);

      // then
      expect(getNotificationByExternalIdSpy).toHaveBeenCalledTimes(1);
      expect(
        getNotificationByExternalIdSpy.mock.calls[0][0],
      ).toMatchInlineSnapshot(`"external-idxx-xxxx-xxxx-xxxxxxxxxxxx"`);
      expect(updateNotificationSpy).toHaveBeenCalledTimes(0);
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(1);
    });

    it("should throw error when updateNotificationById in notification api throws error", async () => {
      // given
      updateNotificationSpy = jest
        .spyOn(notification, "updateNotificationById")
        .mockRejectedValue(internalServerError);

      // when
      await expect(
        processRecord(buildCallBackMessage("delivered")),
      ).rejects.toThrow(internalServerError);

      // then
      expect(getNotificationByExternalIdSpy).toHaveBeenCalledTimes(1);
      expect(
        getNotificationByExternalIdSpy.mock.calls[0][0],
      ).toMatchInlineSnapshot(`"external-idxx-xxxx-xxxx-xxxxxxxxxxxx"`);
      expect(updateNotificationSpy).toHaveBeenCalledTimes(1);
      expect(updateNotificationSpy.mock.calls[0]).toMatchInlineSnapshot(`
        [
          "notifica-tion-idxx-xxxx-xxxxxxxxxxxx",
          {
            "channel": "BATCH",
            "correlation-id": "external-idxx-xxxx-xxxx-xxxxxxxxxxxx",
            "user-id": "CALLBCK_NOTIFY_EVENT",
          },
          {
            "status": "COMPLETED",
          },
        ]
        `);
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(2);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        2,
        "Notification [id: notifica-tion-idxx-xxxx-xxxxxxxxxxxx] retrieved successfully for [externalNotifyId: external-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
      );
    });
  });

  it("should process the sqs record and throw error when notification record not found", async () => {
    // given
    getNotificationByExternalIdSpy = jest
      .spyOn(notification, "getNotificationByExternalId")
      .mockResolvedValue(undefined);

    // when
    await expect(
      processRecord(buildCallBackMessage("delivered")),
    ).rejects.toThrow(new Error("Could not find notification by external id"));

    // then
    expect(getNotificationByExternalIdSpy).toHaveBeenCalledTimes(1);
    expect(
      getNotificationByExternalIdSpy.mock.calls[0][0],
    ).toMatchInlineSnapshot(`"external-idxx-xxxx-xxxx-xxxxxxxxxxxx"`);
    expect(updateNotificationSpy).toHaveBeenCalledTimes(0);
    expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(1);
    expect(winstonErrorLoggerSpy).toHaveBeenCalledTimes(1);
    expect(winstonErrorLoggerSpy).toHaveBeenCalledWith(
      `Notification has not been updated for [externalNotifyId: external-idxx-xxxx-xxxx-xxxxxxxxxxxx] with error: Could not find notification by external id`,
    );
    expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
      1,
      `Processing event [messageId: messagex-idxx-xxxx-xxxx-xxxxxxxxxxxx]`,
    );
  });

  describe("should error when record body is invalid", () => {
    it("should throw an error when sqs record body is unable to parse", async () => {
      // given / when
      await expect(
        processRecord({ ...mockCallBackRecord, body: "invalid" }),
      ).rejects.toThrow("Unable to parse record body: invalid with error");

      // then
      expect(winstonErrorLoggerSpy).toHaveBeenCalledTimes(1);
      expect(winstonErrorLoggerSpy).toHaveBeenCalledWith(
        `Unable to parse record body: invalid with error SyntaxError: Unexpected token i in JSON at position 0`,
      );
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(1);
      expect(winstonInfoLoggerSpy).toHaveBeenCalledWith(
        "Processing event [messageId: messagex-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
      );
    });

    it.each([
      `{"id":""}`,
      `{"id":"external-idxx-xxxx-xxxx-xxxxxxxxxxxx"}`,
      `{"id":"external-idxx-xxxx-xxxx-xxxxxxxxxxxx","status":""}`,
      `{"id":"","status":"delivered"}`,
      `{"id":"","status":""}`,
      `{"status":""}`,
    ])("should throw an error when sqs record body is %p", async (body) => {
      // given / when
      await expect(
        processRecord({ ...mockCallBackRecord, body: body }),
      ).rejects.toThrow("Invalid record body, must contain Id and Status.");

      // then
      expect(winstonErrorLoggerSpy).toHaveBeenCalledTimes(1);
      expect(winstonErrorLoggerSpy).toHaveBeenCalledWith(
        "Invalid record body, must contain Id and Status.",
      );
      expect(winstonInfoLoggerSpy).toHaveBeenCalledTimes(1);
      expect(winstonInfoLoggerSpy).toHaveBeenNthCalledWith(
        1,
        "Processing event [messageId: messagex-idxx-xxxx-xxxx-xxxxxxxxxxxx]",
      );
    });
  });
});

function buildCallBackMessage(status: string) {
  return {
    ...mockCallBackRecord,
    body: `{"id":"external-idxx-xxxx-xxxx-xxxxxxxxxxxx","reference":"12345678","to":"test@test.com","status":"${status}","created_at":"2017-05-14T12:15:30.000000Z","completed_at":"2017-05-14T12:15:30.000000Z","sent_at":"2017-05-14T12:15:30.000000Z","notification_type":"email","template_id":"templatex-idxx-xxxx-xxxxxxxxxxxx","template_version":1}`,
  };
}
