import { handler } from "../index";
import { processRecord } from "../process-sqs-event-record";
import mockEvent from "../../events/event.json";
import mockMultipleRecordsEvent from "../../events/event-multiple-records.json";
import { logger } from "@nhsbsa/hrt-ppc-npm-logging";

jest.mock("../process-sqs-event-record");

const mockProcessRecord = processRecord as jest.MockedFunction<
  typeof processRecord
>;

let winstonErrorLoggerSpy;

beforeEach(() => {
  winstonErrorLoggerSpy = jest.spyOn(logger, "error");
  mockProcessRecord.mockClear();
  mockProcessRecord.mockResolvedValue();
});

describe("handler", () => {
  it("should handle single event successfully without any errors", async () => {
    // given / when
    await expect(handler(mockEvent)).resolves.not.toThrow();

    // then
    expect(mockProcessRecord).toHaveBeenCalledTimes(1);
  });

  it("should handle multiple events successfully without any errors", async () => {
    // given / when
    await expect(handler(mockMultipleRecordsEvent)).resolves.not.toThrow();

    // then
    expect(mockProcessRecord).toHaveBeenCalledTimes(3);
  });

  it("should respond with an internal server error on single record", async () => {
    // given
    const error = new Error("Internal Server Error");
    mockProcessRecord.mockRejectedValueOnce(error);

    // when
    await expect(handler(mockMultipleRecordsEvent)).rejects.toThrow(
      "Internal Server Error",
    );

    // then
    expect(mockProcessRecord).toHaveBeenCalledTimes(3);
    expect(winstonErrorLoggerSpy).toHaveBeenCalledTimes(1);
    expect(winstonErrorLoggerSpy).toHaveBeenCalledWith(error);
  });
});
