import {
  getNotificationByExternalId,
  updateNotificationById,
} from "../notification";
import {
  NotificationApi,
  NotificationPatchData,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import {
  mockNotificationByExternalIdResponse,
  defaultNotificationPatchResponse,
} from "../../__mocks__/mockData";
import { NotificationStatus } from "@nhsbsa/health-charge-exemption-npm-utils-rest/dist/src/clients/notification-models";

let notificationApiSpy: jest.SpyInstance;

const id = "notificationId";
const notificationId = "notificationId";
const headers = {
  header: "value",
};
const data = {} as NotificationPatchData;

beforeEach(() => {
  notificationApiSpy = jest
    .spyOn(NotificationApi.prototype, "makeRequest")
    .mockResolvedValue({});
});

afterEach(() => {
  jest.resetAllMocks();
});

describe("notification", () => {
  describe("getNotificationByExternalId()", () => {
    it("should return the response successfully", async () => {
      // given
      notificationApiSpy.mockResolvedValue(
        mockNotificationByExternalIdResponse,
      );

      // when
      const response = await getNotificationByExternalId(id, headers);

      // then
      expect(response).toBe(mockNotificationByExternalIdResponse);
    });

    it("should call the notification api with correct parameters", async () => {
      // given / when
      await getNotificationByExternalId(id, headers);

      // then
      expect(notificationApiSpy).toHaveBeenCalledTimes(1);
      expect(notificationApiSpy.mock.calls).toMatchInlineSnapshot(`
        [
          [
            {
              "headers": {
                "header": "value",
              },
              "method": "GET",
              "responseType": "json",
              "url": "v1/notifications/search/findByExternalNotifyId?externalNotifyId=${id}",
            },
          ],
        ]
      `);
    });
  });

  describe("updateNotificationById()", () => {
    it("should return the response successfully", async () => {
      // given
      notificationApiSpy.mockResolvedValue(defaultNotificationPatchResponse);

      // when
      const response = await updateNotificationById(
        notificationId,
        headers,
        data,
      );

      // then
      expect(response).toBe(defaultNotificationPatchResponse);
    });

    it("should call the notification api with correct parameters", async () => {
      // given / when
      await updateNotificationById(notificationId, headers, {
        ...data,
        status: NotificationStatus.COMPLETED,
      });

      // then
      expect(notificationApiSpy).toHaveBeenCalledTimes(1);
      expect(notificationApiSpy.mock.calls).toMatchInlineSnapshot(`
        [
          [
            {
              "data": {
                "status": "COMPLETED",
              },
              "headers": {
                "header": "value",
              },
              "method": "PATCH",
              "responseType": "json",
              "url": "/v1/notifications/${id}",
            },
          ],
        ]
      `);
    });
  });
});
