import {
  NotificationApi,
  NotificationPatchData,
  NotificationSearchResponse,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

const notificationApi = new NotificationApi();

export async function getNotificationByExternalId(
  id: string,
  headers,
): Promise<NotificationSearchResponse | undefined> {
  return notificationApi.makeRequest({
    method: "GET",
    url: `v1/notifications/search/findByExternalNotifyId?externalNotifyId=${id}`,
    headers,
    responseType: "json",
  });
}

export async function updateNotificationById(
  notificationId: string,
  headers,
  data: NotificationPatchData,
): Promise<void> {
  return notificationApi.makeRequest({
    method: "PATCH",
    url: `/v1/notifications/${notificationId}`,
    headers,
    data: data,
    responseType: "json",
  });
}
