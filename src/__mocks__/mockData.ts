import {
  CertificateType,
  NotificationSearchResponse,
  NotificationStatus,
  NotificationMethod,
  NotificationPurpose,
} from "@nhsbsa/health-charge-exemption-npm-utils-rest";

export const mockNotificationByExternalIdResponse: NotificationSearchResponse =
  {
    id: "notifica-tion-idxx-xxxx-xxxxxxxxxxxx",
    certificateId: "c0a84475-8b71-1fac-818b-71c02965028d",
    certificateType: CertificateType.HRT_PPC,
    purpose: NotificationPurpose.ISSUE,
    method: NotificationMethod.EMAIL,
    externalNotifyId: "external-idxx-xxxx-xxxx-xxxxxxxxxxxx",
    status: NotificationStatus.PENDING,
    _meta: {},
    _links: {},
  };

export const defaultNotificationPatchResponse = {
  status: 204,
  data: {},
};

export const mockCallBackRecord = {
  messageId: "messagex-idxx-xxxx-xxxx-xxxxxxxxxxxx",
  receiptHandle: "AQEBzWwaftRI0KuVm4tP+/7q1rGgNqicHq...",
  body: "",
  attributes: {
    ApproximateReceiveCount: "1",
    SentTimestamp: "1545082650636",
    SenderId: "AIDAIENQZJOLO23YVJ4VO",
    ApproximateFirstReceiveTimestamp: "1545082650649",
  },
  messageAttributes: {},
  md5OfBody: "e4e68fb7bd0e697a0ae8f1bb342846b3",
  eventSource: "aws:sqs",
  eventSourceARN: "arn:aws:sqs:eu-west-2:123456789012:my-queue",
  awsRegion: "eu-west-2",
};
