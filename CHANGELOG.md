# # Hrt PPC notification external status update Lambda Changelog

## v0.0.0 - 23 Nov 2023

:new: **New features**

- {feature}

:wrench: **Fixes**

- {issue}
